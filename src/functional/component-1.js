import React from 'react';

const Component1 = props =>(
    <div className="component1Data">
        <div>Name : {props.name}</div>
        <div>Age : {props.age}</div>
        <div>city : {props.city}</div>
    </div>
);

export default Component1;