import React from 'react';
import './App.css';
import styles from './styles';
import Component1 from './functional/component-1'
import Component2 from './container/component-2'
import Component3 from './container/Component3'

class App extends React.Component{
  name = 'Lalit'

  render(){
    const age = 30;
    

    return (
      <div className='App'>
        <div style={styles.headingStyle}>Hello {this.name} {age}</div>
        <button>Submit</button>

        <h2>
          Functional Component
        </h2>
        <Component1 name="Lalit Chaudhary" age={31}/>
        <h2>Container Component</h2>
        <Component2 name="Deepak Kumar" age={29} />
        <br/>
        <hr/><hr/>
        <h2>REDUX</h2>
        <hr/><hr/>
        <Component3 />
      </div>
      
    )
  }
}

export default App;
