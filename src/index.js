import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import { provider } from 'react-redux';
import rootReducer from './store/reducers/reducer1';
import { createStore } from 'redux';

let store = createStore(rootReducer)
ReactDOM.render(
            <provider store={store}>
                <App />
            </provider>
            , document.getElementById('root'));
