import React,{ Component } from 'react';
import Component1 from '../functional/component-1'
class Component2 extends Component{
    constructor(props){
        super(props);

        this.state={
            state1 : "Initial State",
            state2 : 0,
            name : ""
        }
    }
    products = [
        {id:1,title:'Product 1',price:'$10'},
        {id:2,title:'Product 2',price:'$12'},
        {id:3,title:'Product 3',price:'$22'},
        {id:4,title:'Product 4',price:'$33'},
        {id:5,title:'Product 5',price:'$11'}
    ]


    changeState = () =>(
        /*this.setState((prevState,props)=>({
            state2:prevState.state2 + 2
        }))*/

        this.setState({
            state2:this.state.state2 + 3
        })
    )

    changeState2 = () => (
        this.setState({
            state1:this.state.state1 + "LC",
            state2:this.state.state2 + 4
        })
    )

    RenderProduct = (props) =>(
        
        <li id={props.product.id}>
            <div>Index: {props.product.id}</div>
            <div>Title: {props.product.title}</div>
            <div>Price: {props.product.price}</div>
        </li>
    )

    handleChange = (event) => {
        console.log(event.target.value)
        this.setState({name:event.target.value})
    }

    handleSubmit = (event) => {
        event.preventDefault();
        console.log(this.state.name)
    }

    render(){
        return(
            <div className="component2Data">
                {this.state.state2 % 2 === 0 ? 
                <div>
                    <h2>CP - 1</h2>
                    <div>Name: {this.props.name}</div>
                    <div>Age: {this.props.age}</div>
                    <div>State: {this.state.state1}</div>
                    <div>State 2: {this.state.state2}</div>
                    <button onClick={()=>this.changeState2()}>Change State 1</button>
                    <button onClick={()=>this.changeState()}>Change State 2</button>
                    <br/>
                    <Component1 name="Lalit" age={32} city={this.state.state1}/>
                </div>
                : 
                <div>
                    <h2>CP - 2</h2>
                    <div>Name: {this.props.name}</div>
                    <div>Age: {this.props.age}</div>
                    <div>State: {this.state.state1}</div>
                    <div>State 2: {this.state.state2}</div>
                    <button onClick={()=>this.changeState2()}>Change State 1</button>
                    <button onClick={()=>this.changeState()}>Change State 2</button>
                    <br/>
                    <Component1 name="Deepak" age={33} city={this.state.state1}/>
                </div>
                }

                <h2>Products</h2>
                <ul>
                    {this.products.map((item,index)=> ( 
                            <this.RenderProduct key={index} product={item}  /> 
                    ))}
                </ul>

                <h2>Form</h2>
                <form onSubmit={this.handleSubmit}>
                    <label>Name</label>
                    <br/>
                    <input type="text" id="name" onChange={this.handleChange} />
                    <br/>
                    {this.state.name}
                    <br/>
                    <button type="submit">Submit</button>
                </form>
            </div>
        )
    }
}

export default Component2;